package Hoot.Compiler;

import java.io.*;
import java.util.*;
import org.apache.commons.cli.*;
import org.apache.commons.cli.Option.*;
import static org.apache.commons.io.FileUtils.*;

import Hoot.Runtime.Faces.*;
import Hoot.Runtime.Maps.Package;
import static Hoot.Runtime.Faces.Utils.*;
import static Hoot.Runtime.Faces.Logging.*;
import static Hoot.Runtime.Names.Primitive.*;
import static Hoot.Runtime.Maps.ClassPath.*;
import static Hoot.Runtime.Maps.Package.*;
import static Hoot.Runtime.Maps.Library.CurrentLib;
import static Hoot.Compiler.Scopes.File.*;

/**
 * Compiles Hoot code to Java classes and types.
 * Takes command line arguments and thereby instructs the compiler.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public class HootMain implements Logging {

    private File baseFolder;
    private File sourceFolder;
    private File targetFolder;
    private File classFolder;

    private CommandLine command;
    private final CommandLineParser parser = new DefaultParser();
    private List<String> packageList = new ArrayList();

    /**
     * Compiles the Hoot files located in the configured source folder.
     * @param args command line arguments
     */
    public static void main(String[] args) {
        HootMain c = new HootMain();
        c.parseCommand(args);

        if (c.needsHelp()) {
            c.printHelp();
        }
        else {
            c.compile();
        }
    }

    /**
     * Compiles the Hoot located in the configured folders.
     */
    public void compile() {
        if (command == null) return;

        prepareFolders();
        if (packageList.isEmpty()) {
            compileAll(sourceFolder, targetFolder);
        }
        else {
            compileAllPackages();
        }
    }

    private HootMain() { }
    private void parseCommand(String[] args) {
        try {
            command = parser.parse(availableOptions(), args);
            packageList = wrap(command.getOptionValues(Packages));
        }
        catch (ParseException ex) {
            error(ex.getMessage());
            printHelp();
        }
    }

    private void printHelp() {
        HelpFormatter f = new HelpFormatter();
        f.printHelp(Compiler, availableOptions());
    }

    private Options availableOptions() {
        Options result = new Options();
        result.addOption(baseOption());
        result.addOption(sourceOption());
        result.addOption(targetOption());
        result.addOption(packageOption());
        result.addOption(cleanOption());
        result.addOption(helpOption());
        return result;
    }

    private boolean needsHelp() {
        if (command == null) return false;
        return command.hasOption(Help);
    }

    private boolean cleansFolders() {
        if (command == null) return false;
        return command.hasOption(Clean);
    }

    private void prepareFolders() {
        String basePath = systemValue(BasePath);
        if (command.hasOption(Base)) {
            basePath = command.getOptionValue(Base);
        }

        String sourcePath = command.getOptionValue(Source, SourcePath);
        String folderPath = command.getOptionValue(Folder, TargetPath);
        report(Empty);

        // validate the paths
        baseFolder = locate(BasePath, basePath);
        if (baseFolder == null) return;

        sourceFolder = locate(Source, sourcePath);
        targetFolder = locate(Target, folderPath, TargetPath);
        classFolder  = locate(Binary, folderPath, ClassesPath);

        File[] folders = { sourceFolder, targetFolder, classFolder };
        List<File> basicPaths = new ArrayList(wrap(folders));

        if (basePath.endsWith("code-samples")) {
            basicPaths.add(locate(Adding, "../libs-hoot", ClassesPath));
        }

        if (basePath.endsWith("code-hoot")) {
            basicPaths.add(locate(Adding, "../libs-smalltalk", ClassesPath));
        }

        basicPaths.add(locate(Adding, "../hoot-runtime", ClassesPath));
        basicPaths.add(locate(Adding, "../hoot-abstracts", ClassesPath));

        CurrentPath.loadPaths(basicPaths);
        UnitFactory = StandardUnitFactory;
    }

    private File locate(String pathName, String... folderPaths) {
        File result = new File(folderPaths[0]);

        if (!BasePath.equals(pathName)) {
            pathName = Blank + Blank + pathName;
            if (!result.isAbsolute()) {
                result = new File(baseFolder.getAbsolutePath(), folderPaths[0]);
                if (folderPaths.length > 1) {
                    result = new File(result.getAbsolutePath(), folderPaths[1]);
                }
            }
        }

        if (result.exists()) {
            report(format(FolderFound, pathName, result.getAbsolutePath()));
            return result;
        }
        else if (Target.equals(pathName)) {
            result.mkdirs();
            return result;
        }
        else {
            error(format(FolderFailed, result.getAbsolutePath()));
            return null;
        }
    }

    private void compileAllPackages() {
        packageList.forEach((packageName) -> {
            compilePackageNamed(packageName);
        });
    }

    private void compileAll(File hootFolder, File javaFolder) {
        List<String> skippedFolders = wrap(Skipped);
        compileOnly(hootFolder, javaFolder);

        File[] subFolders = hootFolder.listFiles(FolderFilter);
        for (File folder : subFolders) {
            String folderName = folder.getName();
            if (!skippedFolders.contains(folderName)) {
                File resultFolder = new File(javaFolder, folderName);
                compileAll(new File(hootFolder, folderName), resultFolder);
            }
        }
    }

    private void compileOnly(File hootFolder, File javaFolder) {
        File[] hootFiles = hootFolder.listFiles(HootFilter);
        if (hootFiles.length > 0) {
            String packageName = Package.nameFrom(targetFolder, javaFolder);
            if (packageList.isEmpty() || packageList.contains(packageName)) {
                compilePackageNamed(packageName);
            }
        }
    }

    private void compilePackageNamed(String packageName) {
        report(Empty);
        cleanUp(packageName);
        report(format(Translation, packageName));
        try {
            Map<String, UnitFile> fileMap = Package.named(packageName).parseSources();
            CurrentLib.loadEmptyPackages();
            fileMap.values().forEach(f -> f.compile());
        }
        catch (Throwable ex) {
            // seriously jacked! --nik
            logger().error(ex.getMessage(), ex);
        }
    }

    private void cleanUp(String packageName) {
        File javaFolder = Package.named(packageName).targetFolder();
        if (javaFolder.exists() && cleansFolders()) {
            report(format(Cleaning, packageName));
            deleteQuietly(javaFolder);
        }

        if (!javaFolder.exists()) {
            javaFolder.mkdirs();
        }
    }

    private static String shortOption(String optionName) {
        return optionName.substring(0, 1);
    }

    private Builder buildOption(String optionName, String text) {
        return buildOption(optionName, Empty, text);
    }

    private Builder buildOption(String optionName, String argName, String text) {
        return Option.builder(shortOption(optionName))
                .longOpt(optionName).required(false).desc(text)
                .argName(argName.isEmpty() ? optionName+Optioned : argName);
    }

    private Option baseOption() {
        return buildOption(Base, Base+Path, "a path to the project base folder").hasArg().build();
    }

    private Option sourceOption() {
        return buildOption(Source, Source+Path, "a path to the Hoot sources folder").hasArg().build();
    }

    private Option targetOption() {
        return buildOption(Folder, Target+Path, "a path to the target Java folder").hasArg().build();
    }

    private Option cleanOption() {
        return buildOption(Clean, "removes any previously generated code").build();
    }

    private Option packageOption() {
        return buildOption(Packages, PackNames, "specific packages to compile")
                .valueSeparator(Blank.charAt(0)).hasArgs().build();
    }

    private Option helpOption() {
        return buildOption(Help, "displays this help").build();
    }

    static final String Dot = ".";
    static final String Comma = ",";
    static final String Blank = " ";
    static final String Slash = "/";
    static final String Dash = "-";
    static final String Optional = Dash + Dash;
    static final String Bounds = "[]";

    static final String Help = "help";
    static final String Clean = "clean";
    static final String Base = "base";
    static final String Source = "source";
    static final String Folder = "folder";
    static final String Target = "target";
    static final String Binary = "binary";
    static final String Adding = "adding";
    static final String Packages = "packages";
    static final String PackNames = "packageNames";

    static final String Path = "Path";
    static final String Optioned = "Option";

    static final String BasePath = "user.dir";
    static final String SourcePath = "src/main";
    static final String TargetPath = "src/main/java";
    static final String ClassesPath = "target/classes";
    static final String Compiler = "hoot-compiler";
    static final String[] Skipped = { "resources", "java" };

    static final String HootFileType = ".hoot";
    static final String JavaFileType = ".java";

    static final String FolderFound = "%s folder = %s";
    static final String FolderFailed = "can't locate %s";
    static final String Translation = "translating %s";
    static final String Cleaning = "cleaning %s";


    static final FileFilter HootFilter = (File candidate) -> isHoot(candidate);
    static final FileFilter FolderFilter = (File candidate) -> isFolder(candidate);

    private static boolean isFolder(File candidate) {
        return candidate.isDirectory();
    }

    private static boolean isHoot(File candidate) {
        return candidate.isFile() && candidate.getPath().endsWith(HootFileType);
    }


    /**
     * @param folderPath a folder path
     * @param packages a package name list
     * @return arguments for a clean compiler command
     */
    public static String[] buildCleanCommand(String folderPath, String... packages) {
        return buildCommand(folderPath, packages, Optional + Clean);
    }

    /**
     * @param folderPath a folder path
     * @param packages a package name list
     * @param options some compiler option(s)
     * @return arguments for a compiler command
     */
    public static String[] buildCommand(String folderPath, String[] packages, String... options) {
        List<String> results = new ArrayList();
        String packageList = wrap(packages).toString();
        packageList = strip(packageList, Bounds).replace(Comma, Empty);

        String[] args = {
            Dash + shortOption(Folder),
            folderPath,
            Dash + shortOption(Packages),
            packageList,
        };

        List<String> argList = wrap(args);
        List<String> optionList = wrap(options);

        results.addAll(argList);
        results.addAll(optionList);

        String[] empty = { };
        return unwrap(results, empty);
    }

} // HootMain
