package Hoot.Compiler.Constants;

import Hoot.Runtime.Emissions.*;
import Hoot.Runtime.Faces.Named;

/**
 * A named value.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public class NamedValue extends Constant implements Named {

    public NamedValue() { super(); }
    public NamedValue(String name, Item value) { this(); this.name = name; this.value = value; }
    public static NamedValue with(String name, Item value) { return new NamedValue(name, value); }

    protected String name;
    @Override public String name() { return name; }

    protected Item value;
    @Override public Item value() { return value; }
    public Scalar scalarValue() { return (Scalar)value(); }

    @Override public Emission emitItem() {
        return (name().isEmpty()) ? value().emitItem() : emitPair(name(), value().emitItem()); }

} // NamedValue
