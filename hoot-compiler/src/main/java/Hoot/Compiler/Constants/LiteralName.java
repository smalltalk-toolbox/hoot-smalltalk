package Hoot.Compiler.Constants;

import Hoot.Runtime.Faces.Named;
import Hoot.Runtime.Emissions.*;
import Hoot.Runtime.Values.Variable;
import static Hoot.Runtime.Names.Keyword.*;
import static Hoot.Runtime.Faces.Utils.*;
import Hoot.Runtime.Names.TypeName;
import static Hoot.Runtime.Names.TypeName.EmptyType;

/**
 * A reference to self, super, or some scoped variable.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public class LiteralName extends Constant implements Named {

    public LiteralName() { super(); }
    public LiteralName(String name) { super(); this.name = name; }
    public static LiteralName with(String name, int line) { return new LiteralName(name).withLine(line); }
    public static LiteralName with(String name) { return new LiteralName(name); }
    public static LiteralName literalSelf() { return with(Self); }

    protected String name = Empty;
    @Override public String name() { return this.name; }
    @Override public boolean selfIsPrimary() { return referencesSelf(); }
    public boolean referencesSelf() { return name().equals(Self); }
    public boolean referencesSuper() { return name().equals(Super); }
    public boolean referencesLocal() { return matchAny(blockScopes(), s -> s.hasLocal(name())); }
    public Variable referencedLocal() { 
        return nullOr(s -> s.localNamed(name()), findFirst(blockScopes(), bs -> bs.hasLocal(name()))); }

    @Override public TypeName typeResolver() {
        if (name().equals("dict")) {
            whisper(Empty);
        }
        if (referencesLocal()) {
            return referencedLocal().typeResolver();
        }
        return EmptyType;
    }

    private Emission emitNamedValue(Variable v) { return emitNamedValue(name(), v.emitType(false)); }

    @Override public Emission emitOperand() {
        if (referencesSelf()) return emitSelf();
        if (referencesSuper()) return emitSuper();

        if (containerScope().isFacial()) {
            return emitItem(name());
        }

        if (referencesLocal()) {
            Variable v = referencedLocal();
            if (v.isStacked()) return emitNamedValue(v);
        }

        return emitItem(name());
    }

} // LiteralName
