package Hoot.Compiler.Scopes;

import java.io.*;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

import Hoot.Compiler.*;
import static Hoot.Compiler.HootParser.*;
import static Hoot.Compiler.Notes.Comment.*;
import Hoot.Runtime.Emissions.FreeMarkup;
import Hoot.Runtime.Faces.Logging;

/**
 * Compiles a Hoot file from a token stream with a parser.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public class TokenCompiler implements Logging {

    File tokenFile;
    public TokenCompiler(File aFile) { tokenFile = aFile; }
    private String tokenFilepath() { return tokenFile.sourceFile().getAbsolutePath(); }
    private CharStream createInputStream() throws Exception { return new ANTLRFileStream(tokenFilepath()); }
    private HootParser createParser() throws Exception { return new HootParser(createTokenStream()); }
    private TokenSource createLexer() throws Exception { return new HootLexer(createInputStream()); }

    CommonTokenStream tokenStream;
    public CommonTokenStream tokenStream() { return tokenStream; }
    private CommonTokenStream tokenStream(CommonTokenStream stream) { this.tokenStream = stream; return tokenStream; }
    private TokenStream createTokenStream() throws Exception { return tokenStream(new CommonTokenStream(createLexer())); }

    public boolean compile() {
        java.io.File sourceFile = tokenFile.sourceFile();
        if (sourceFile == null || !sourceFile.exists()) {
            reportMissingSource();
            return false;
        }

        parseTokens();
        if (wasParsed()) {
            emitCode();
            return true;
        }
        else {
            return false;
        }
    }

    HootParser parser;
    public boolean wasParsed() { return parser != null; }
    public boolean notParsed() { return parser == null; }

    CompilationUnitContext resultUnit;
    public void parseTokens() {
        if (wasParsed()) return;

        try {
            tokenFile.makeCurrent();
            report("parsing " + tokenFile.name());

            parser = createParser();
            resultUnit = parser.compilationUnit();
            tokenFile.cacheComments(buildComments(tokenStream()));
            ParseTreeWalker walker = new ParseTreeWalker();
            walker.walk(new HootBaseListener(), resultUnit);
        }
        catch (Exception ex) {
            // unlikely given safeguards
            error(ex.toString());
            ex.printStackTrace();
        }
        finally {
            tokenFile.popScope();
        }
    }

    private void emitCode() {
        tokenFile.clean();
        java.io.File targetFolder = tokenFile.facePackage().createTarget();
        if (targetFolder == null) return; // failure already reported

        report("writing " + tokenFile.targetFilename());
        java.io.File targetFile = new java.io.File(targetFolder, tokenFile.targetFilename());
        try (FileWriter oStream = new FileWriter(targetFile)) {
            FreeMarkup.named(tokenFile.faceScope().codeFile())
                .with(tokenFile).with(tokenFile.faceScope()).injectValues(oStream);
        }
        catch (Exception ex) {
            error(ex.getMessage(), ex);
        }
    }

    private void reportMissingSource() {
        String message = "can't find source file for " + tokenFile.faceName();
        error(message);
    }

} //  TokenCompiler
