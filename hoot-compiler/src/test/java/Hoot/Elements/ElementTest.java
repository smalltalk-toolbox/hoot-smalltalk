package Hoot.Elements;

import java.util.*;
import java.lang.reflect.*;
import org.junit.*;

import Hoot.Compiler.Scopes.Face;
import Hoot.Runtime.Emissions.FreeMarkup;
import Hoot.Runtime.Behaviors.Typified;
import static Hoot.Runtime.Maps.ClassPath.*;
import Hoot.Runtime.Faces.Logging;

/**
 * Confirms proper operation of certain essential elements.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public class ElementTest implements Logging {

    private static final String SourceFolder = "../code-smalltalk/src/main/Smalltalk";
    private static final String TargetFolder = "../libs-smalltalk/src/main/java";
    private static final String ClassFolder  = "../libs-smalltalk/target/classes";
    private static final String[] BasePaths = { SourceFolder, TargetFolder, ClassFolder, };

    @Test
    @Ignore
    public void sampleTest() {
        report(markup().injectValues().trim());
        report(markup().with("sample_text", "a markup example").injectValues().trim());
    }

    @Test public void variableTest() {
        loadBasePaths(BasePaths);
        Typified sample = Face.from(Face.class);

        report(sample.fullName());
        report(sample.instanceFields());
        report(Empty);
        report(sample.staticFields());

//        report(Empty);
//        Map<String, Field> map = sample.staticFields();
//        Variable v = Variable.from(map.get("ClassType"));
//        report(v.description());
//        report(markup().with(v).injectValues().trim());
    }

    private FreeMarkup markup() {
        return FreeMarkup.named("sample.ftl");
    }

    static final String FieldReport = "  %s: %s";
    private void report(Map<String, Field> fieldMap) {
        fieldMap.forEach((name,field) -> {
            report(format(FieldReport, name, field.getType().getCanonicalName()));
        });
    }

}
