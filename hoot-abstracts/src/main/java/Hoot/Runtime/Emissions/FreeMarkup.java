package Hoot.Runtime.Emissions;

import java.io.*;
import java.util.*;
import freemarker.template.*;
import freemarker.cache.ClassTemplateLoader;
import static org.apache.commons.lang3.StringUtils.*;

/**
 * Wraps the FreeMarker library.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public class FreeMarkup {

    public static FreeMarkup named(String fileName) {
        FreeMarkup result = new FreeMarkup();
        result.configuration = configureMarkup();
        result.templateFile = fileName;
        return result;
    }

    public static Configuration configureMarkup() {
        Configuration result = new Configuration(MarkupVersion);
        result.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        result.setTemplateLoader(new ClassTemplateLoader(FreeMarkup.class, "/templates"));
        result.setObjectWrapper(wrapperBuilder().build());
        result.setDefaultEncoding(Encoding);
        return result;
    }

    private static DefaultObjectWrapperBuilder wrapperBuilder() {
        DefaultObjectWrapperBuilder result = new DefaultObjectWrapperBuilder(MarkupVersion);
        result.setDefaultDateType(TemplateDateModel.DATETIME);
        result.setForceLegacyNonListCollections(false);
        return result;
    }

    private String templateFile = Empty;
    private Configuration configuration;
    private final HashMap<String, Object> map = new HashMap<>();

    public FreeMarkup with(Object... valueRoots) {
        for (Object value : valueRoots) {
            if (value != null) map.put(getName(value), value);
        }
        return this;
    }

    public String injectValues() {
        try (StringWriter buffer = new StringWriter()) {
            injectValues(buffer);
            buffer.flush();
            return buffer.toString();
        }
        catch (Exception ex) {
            return Empty;
        }
    }

    public void injectValues(Writer buffer) throws Exception { getTemplate().process(map, buffer); }
    public FreeMarkup with(String valueName, Object value) { map.put(valueName, value); return this; }
    private String getName(Object value) { return uncapitalize(value.getClass().getSimpleName()); }
    private Template getTemplate() throws Exception { return configuration.getTemplate(templateFile); }

    static final String Empty = "";
    static final String Encoding = "UTF-8";
    static final Version MarkupVersion = Configuration.VERSION_2_3_27;

} // FreeMarkup