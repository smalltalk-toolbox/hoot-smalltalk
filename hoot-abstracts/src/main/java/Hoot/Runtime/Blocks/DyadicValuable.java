package Hoot.Runtime.Blocks;

import java.util.Comparator;
import Hoot.Runtime.Faces.Valued;
import Hoot.Runtime.Faces.ConditionalValue;

/**
 * A dyadic block closure protocol.
 * Defines the type signature for ANSI Smalltalk DyadicValuable (section 5.4.6).
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public interface DyadicValuable extends Arguable {

    /**
     * @return the result when evaluated with 2 values
     * @param <R> a result type
     * @param <A> 1st value type
     * @param <B> 2nd value type
     * @param a 1st value
     * @param b 2nd value
     */
    public <A, B, R> R value_value(A a, B b);

    /**
     * @return a Comparator for this dyadic valuable
     */
    public default Comparator asComparator() {
        return new java.util.Comparator() {
            @Override public int compare(java.lang.Object a, java.lang.Object b) {
                if (a != null && a.equals(b)) return 0;
                ConditionalValue v = (ConditionalValue)value_value((Valued)a, (Valued)b);
                return( v.primitiveBoolean() ? -1 : 1 ); }

            @Override public boolean equals(java.lang.Object aComparator) {
                return (aComparator != null && aComparator instanceof Comparator && this == aComparator); }
        };
    }

} // DyadicValuable
