@Notice :'Copyright 2010,2019 Nikolas S Boyd.
Permission is granted to copy this work provided this copyright statement is retained in all copies.
See https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt for more details.'!

Hoot Behaviors Boolean import.
Smalltalk Blocks Predicate import.
Smalltalk Magnitudes Ratio import.
Smalltalk Magnitudes Scalar import.
Smalltalk Magnitudes Numeric import.

Number subclass: @Abstract! Ratio! Rational.
"Represents a ratio between a numerator and a denominator."

Rational class members: []

Rational members: "accessing values"
[
    "Returns the numerator."
    @Abstract! Integer! numerator []

    "Returns the denominator."
    @Abstract! Integer! denominator []

    @Protected! Rational [ super. ]
]

Rational members: "comparing values"
[
    Boolean! = Rational! aNumber "whether (this = aNumber)" [
    	^self numerator = aNumber numerator and:
            [ self denominator = aNumber denominator ]
    ]

    Boolean! < Rational! aNumber "whether (this < aNumber)" [
    	^(self numerator * aNumber denominator) < (self denominator * aNumber numerator)
    ]

    Boolean! > Rational! aNumber "whether (this > aNumber)" [
    	^(self numerator * aNumber denominator) > (self denominator * aNumber numerator)
    ]

    @Override! @Primitive! Boolean! = Numeric! aNumber "whether (this = aNumber)" [
        Number! n := Number <- aNumber.
    	^(self lessGeneral: n)
            ifTrue: [ n equals: self ]
            ifFalse: [ self equals: n asRational ]
    ]

    @Primitive! Boolean! < Number! aNumber "whether (this < aNumber)" [
    	^(self lessGeneral: aNumber)
            ifTrue: [ aNumber moreThan: self ]
            ifFalse: [ self lessThan: aNumber asRational ]
    ]

    @Primitive! Boolean! > Number! aNumber "whether (this > aNumber)" [
    	^(self lessGeneral: aNumber)
            ifTrue: [ aNumber lessThan: self ]
            ifFalse: [ self moreThan: aNumber asRational ]
    ]

    @Primitive! Int! hashCode [
        "a value to use for equivalence testing"
        ^(self numerator bitXor: self denominator) intValue
    ]
]

Rational members: "converting values"
[
    asRational [ ^self ]
]