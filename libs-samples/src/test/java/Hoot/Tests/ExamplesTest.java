package Hoot.Tests;

import org.junit.*;
import Hoot.Examples.*;
import Hoot.Runtime.Faces.Logging;

/**
 * Confirms proper operation of provided Hoot examples.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public class ExamplesTest implements Logging {

    static final String[] NoArgs = { };
    @Test public void hanoiTest()    { SimpleHanoi.main(NoArgs); }
    @Test public void helloWorld()   { HelloWorld.main(NoArgs); }
    @Test public void literalsTest() { LiteralsTest.main(NoArgs); }
    @Test public void integersTest() { IntegersTest.main(NoArgs); }
    @Test public void geometryTest() { GeometryTest.main(NoArgs); }
    @Test public void sticTest()     { SticBenchmark.main(NoArgs); }

} // ExamplesTest
