#### Blocks ####

Blocks are a very powerful part of the [Smalltalk][smalltalk] language.
They are so important, Hoot not only retains the block concepts of Smalltalk,
but also extends them in thoughtful ways to improve integration with its host platforms.
Blocks are so flexible, Hoot uses them for a wide variety of language features,
including decision structures, collection iteration, exception handling, multi-threading, and event handling,
in ways that Smalltalk developers will find most familiar.

#### Decision Structures ####

Like Smalltalk, there are no reserved words for decision structures in Hoot like there are in
languages such as [Java][java] and [C#][csharp].
Instead, decision structures use message idioms that combine Boolean expressions with Blocks.
The table below lists some of the most commonly used decision idioms as they are expressed in Hoot.

Notice that the decision idioms do not include a **switch** or a **case** statement.
There are ways to mimic such a control structure in Hoot.
However, that approach is generally discouraged in favor of object-oriented designs that make use of
classes and polymorphism to distinguish and handle the separation of cases.

Many of the decision structures identified below can be optimized during translation to a host language (Java or C#).
Often, they can be translated directly into equivalent decision structures.
Similar optimizations are often performed by commercial Smalltalk compilers.
However, under certain circumstances, these control structures and other custom blocks are best
implemented using closures (lambdas).

| Idiom         | Examples     |
|---------------|--------------|
| Decisions     | result := boolValue `or:` [ aBoolean ].   |
|               | result := boolValue `and:` [ aBoolean ].  |
| Alternatives  | result := boolValue `ifTrue:` [ "..." ].  |
|               | result := boolValue `ifTrue:` [ "..." ] `ifFalse:` [ "..." ].  |
|               | result := boolValue `ifFalse:` [ "..." ].  |
|               | result := boolValue `ifFalse:` [ "..." ] `ifTrue:` [ "..." ].  |
| Loops         | [ boolValue ] `whileTrue:` [ "..." ]. |
|               | [ boolValue ] `whileFalse:` [ "..." ]. |
|               | [ "..." boolValue ] `whileTrue`. |
|               | [ "..." boolValue ] `whileFalse`. |
| Intervals     | start `to:` end `do:` [ :index \| "..." ]. |
|               | start `to:` end `by:` delta `do:` [ :index \| "..." ]. |
| Collections   | elements `do:` [ :element \| "..." ]. |
|               | results := elements `collect:` [ :element \| "..." ]. |
|               | results := elements `select:` [ :element \| "..." ]. |
|               | results := elements `reject:` [ :element \| "..." ]. |
|               | results := elements `collect:` [ :element \| "..." ]. |
|               | result := elements `detect:` [ :element \| "..." ]. |
|               | result := elements `detect:` [ :element \| "..." ] `ifNone:` [ "..." ]. |
|               | result := elements `inject:` initialValue `into:` [ :aValue :element \| "..." ]. |
| Evaluations   | result := [ :a :b \| "..." ] `value:` x value: y. |
|               | result := [ :a \| "..." ] `value:` x. |
|               | result := [ "..." ] `value`. |
| Exceptions    | [ "..." ] `ifCurtailed:` [ "... catch any exception ..." ]. |
|               | [ "..." ] `ensure:` [ "... evaluated finally ..." ]. |
|               | [ "..." ] `catch:` [ : ExceptionType! ex \| "..." ] `ensure:` [ "... evaluated finally ..." ]. |
| Threads       | [ "..." ] `fork`. |
|               | [ "..." ] `forkAt:` aPriority. |
| Synchronizing | subject `lockDuring:` [ "..." ]. |
|               | subject `notifyOneWaitingThread`. |
|               | subject `notifyAllWaitingThreads`. |
|               | subject `wait:` msecsDuration `ifInterrupted:` [ "..." ]. |
| Adapters      | InterfaceType `asNew:` [ "..." ] |

#### Implementing Blocks ####

Early experiments with Bistro mapped Smalltalk blocks to anonymous inner classes derived from Block base classes.
A similar approach is taken with Hoot.
However, with the advent of lambdas in Java SE 8, Hoot has a simpler mapping in how blocks are translated and used.
Largely, this results from the elimination of some boiler plate elements needed for inner classes.
Java lambdas have their analog in certain kinds of Smalltalk block closures.
So, translating those becomes syntactically much simpler.

```smalltalk
sortBlock [ "orders a comparable pair of text elements"
    ^[ : String! a : String! b | a <= b ]
]
```

#### Method Returns from Blocks ####

Like Smalltalk, Hoot supports the ability to return method results directly from inside
nested blocks using a message expression that begins with a caret **^**.
Method returns exit all enclosing block scopes, including the enclosing method scope.

The following `search:` method provides an example of this feature.
If the method finds any of the **searchTargets** in **aCollection**, it returns the element as the result of the method.
Note that in this case, the method result is returned by an exit from a nested block scope.

```smalltalk
search: aCollection for: searchTargets [
    aCollection do: [ :element |
        (searchTargets includes: element)
            ifTrue: [ ^element ] "<-- note method exit"
    ].
    ^nil
]
```

Early experiments with [Bistro][bistro] mapped method exits from nested scopes to Java using an exception based mechanism.
While convenient, this approach impacted performance, as exception handling tends to be slower than simple method
invocation and return. Further consideration may be given to this in a future version of Hoot.

When translating methods, the Hoot compiler code generator automatically inserts a stack **Frame** in every generated
method scope.
The Hoot runtime might take advantage of this to provide a better method exit mechanism.
However, whether this approach can be made to work any better needs some further experiments.

```
Copyright 2010,2019 Nikolas S Boyd.
Permission is granted to copy this work provided this copyright statement is retained in all copies.
```
See https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt for LICENSE details.


[smalltalk]: https://en.wikipedia.org/wiki/Smalltalk "Smalltalk"
[images]: https://en.wikipedia.org/wiki/Smalltalk#Image-based_persistence "Image Persistence"
[java]: https://en.wikipedia.org/wiki/Java_%28programming_language%29 "Java"
[csharp]: https://en.wikipedia.org/wiki/C_Sharp_%28programming_language%29 "C#"
[antlr]: https://www.antlr.org/ "ANTLR"
[st]: https://www.stringtemplate.org/ "StringTemplate"
[git]: https://git-scm.com/ "Git"
[github]: https://github.com/ "GitHub"
[nexus]: https://www.sonatype.com/nexus "Sonatype Nexus"
[generics]: https://en.wikipedia.org/wiki/Parametric_polymorphism "Generic Types"

[bistro]: https://bitbucket.org/nik_boyd/bistro-smalltalk/ "Bistro"
[except]: exceptions.md "Exceptions"
