### Frequently Asked Questions ###

* [Why run Smalltalk on the JVM and CLR?](#why-run-smalltalk-on-the-jvm-and-clr)
* [Why is Hoot different from Smalltalk?](#why-is-hoot-different-from-smalltalk)
* [What are the similarities and differences between Hoot and Smalltalk?](#what-are-the-similarities-and-differences-between-hoot-and-smalltalk)

#### Why run Smalltalk on the JVM and CLR? ####

[Smalltalk][smalltalk] and [Java][java] both have their technical merits.
If you look at the history of its technology, many of the innovations found in Java trace their conceptual origins back to Smalltalk.
Smalltalk was the first language and tool set to bring fully (pure) object-oriented programming into the mainstream.

Together, Smalltalk and the Java ecosystem provide a powerful combination.
Likewise for C#, which also derives much of its technical foundations from both Java and Smalltalk.
From a certain point of view, the integration of Smalltalk with these platforms seems inevitable.

**1. Smalltalk is a better modeling language than Java and C#.**

Smalltalk is far more expressive and readable than Java and C#, partly due to Smalltalk's simplicity.
But also, as I indicated in [Using Natural Language in Software Development][using-natural-language],
Smalltalk provides a natural way to model the elements of natural language problem descriptions
by directly supporting the linguistic metaphors we use in object-oriented software designs.
Thus, Smalltalk can provide superior traceability from code back to the original solution requirements.

With its blend of elements from both Smalltalk and Java, Hoot may better express notions
of [Enduring Business Themes and Business Objects][stable-concept],
while Java and C# may better encode [Industrial Objects][stable-concept].

**2. Smalltalk has first-class meta-classes, while Java does not (nor does C#).**

Object-oriented designs often need meta-protocols for factory and registry methods.
Object-oriented frameworks with meta-protocols often make polymorphic meta-protocols desirable.
Java `static` methods support a kind of inheritance, but without full polymorphism.
So, Java does not support full meta-design and meta-programming.
Like Smalltalk, Hoot provides first-class meta-classes, which support both inheritance and full polymorphism
for the design of meta-protocols.

**3. Java and C# provide name spaces and standardized binary distribution mechanisms.**

Smalltalk has no standardized [name space](libs.md#name-spaces) mechanism.
This can produce class naming conflicts, and it can complicate the integration of class libraries from disparate origins.
Java and C# both have name space mechanisms built into their languages and VMs.

Smalltalk has no standardized binary distribution mechanism for its classes.
While Smalltalk's image-based development model contributes to its agility and productivity,
it complicates final product packaging and deployment.
Java provides several options for packaging and deploying its binary artifacts, including Java archives - JARs,
Web archives - WARs, Enterprise archives - EARs, etc.
C# has its class binaries combined into assemblies as Dynamic Link Libraries - DLLs.

**4. The JVM and CLR combined offer relative ubiquity for deployment. LLVM too?**

LLVM needs more thought and experiment. However, given the relative ubiquity of the JVM (esp. with [OpenJDK][open-jdk])
and CLR (esp. with [.NET Core][net-core]), and their availability across many platforms, it makes most sense to target
these platforms.

#### Why is Hoot different from Smalltalk? ####

Many ideas found in Hoot were pioneered in [Bistro][bistro].
Much like Bistro, the design of Hoot was driven by the following goals:

* Retain much of the simplicity, expressiveness, and readability of Smalltalk
* Compile Hoot code to its supported host platform binaries
* Leverage the VM and language advances made in the supported host platforms
* Provide seamless integration with existing class libraries
* Support the standard [ANSI Smalltalk][hoot-ansi] protocols
* Optimize with direct method calls as much as possible
* Support @**Primitive** methods written in Hoot

Using annotations uniformly for both code decorations and host language annotations provided a major simplification
of the Hoot syntax over that offered by [Bistro][bistro].
Using more of the available punctuation for type declarations permitted better support for [optional typing][optional],
better support for casting, and better definition and usage of [generic types][generics].

Simple punctuation **( @ ! ^ <- -> ... [ ] )** makes these decorations less unobtrusive in the code,
retaining readability, while gaining the power of these additions to the language model:

* Uniform Annotations
* Optional Types
* Generic Types
* Type Casting
* Argument Lists

#### What are the similarities and differences between Hoot and Smalltalk? ####

There are many similarities between Hoot and Smalltalk, but also some differences.

**1. Hoot supports Smalltalk message syntax, with some extensions.**

To retain the expressiveness and readability of Smalltalk, Hoot uses Smalltalk's syntax for
unary, binary, and keyword messages.
However, to support seamless integration with Java, Hoot extends the keyword message syntax with
simple argument lists.
Thus, keyword messages can include trailing arguments separated by colons, as in the following example.

```smalltalk
Point basicNew: 0 : 0
```

The Hoot compiler translates the expression above into the following Java code.

```java
new Point(0, 0)
```

**2. Hoot blocks and methods resemble Smalltalk blocks and methods.**

Smalltalk blocks and methods have many similarities.
However, Smalltalk blocks contain their signatures,
while method signatures precede their bodies.
So, while blocks have delimiters **[ ]**, standard Smalltalk method bodies don't.
This was one of the factors that originally drove the design of the Smalltalk [chunk file format][chunk-files].
When represented in a flat file, Smalltalk method definitions need some kind of delimiter - hence the use of bang **!**
to mark the end of each Smalltalk method.

```smalltalk
"sample Smalltalk method (chunk)"
yourself
    ^self !
```

Hoot resolves these differences with a uniform syntax: all scopes are delimited with square brackets **[ ]**.

```smalltalk
"sample Hoot method"
yourself [ ^self ]
```

This simple change in syntax eliminates the need for code chunks and allows for a declarative class definition format.

**3. Hoot variables can be defined and initialized together.**

Hoot variables can be initialized when they first appear within a scope.
This includes instance and `static` variables associated with a class (or type), and local variables
within a method scope or a block scope.

```smalltalk
sampleVariable := 5.
```

This resembles the syntax of Java, and it's somewhat simpler than the corresponding syntax for Smalltalk.

**4. Hoot supports optional type annotations.**

While not required, variables and arguments may also have associated type annotations.

```smalltalk
SmallInteger! sampleVariable := 5.
```

**5. Hoot type declarations are messages, with associated type members.**

Unlike Smalltalk, Hoot supports the definition of types separate from classes.
Hoot types (and meta-types) get translated into Java interfaces.
Here's a sample from the **Smalltalk Magnitudes** package.

```
Numeric subtype: Ratio. "Smalltalk Rational type (ANSI X3J20 section 5.6.3)."

Ratio type members: [ ]
Ratio "accessing" members:
[
    Ordinal! numerator "the numerator of this ratio" []
    Ordinal! denominator "the denominator of this ratio" []
]
```

This sample shows how **Ratio** is a simple extension subtype of **Numeric**.
Note that all method signatures in Hoot type definitions have empty method scopes **[]**.
Both @**Abstract** and @**Native** methods also have [empty method scopes](methods.md#native-and-abstract-methods).

Note also that **Ratio** has (an empty) meta-type **Ratio type**.
The Hoot compiler then also generates a nested meta-type in Java named **Ratio Metatype** which extends **Numeric Metatype**.

The [ANSI Smalltalk][squeak-ansi] protocols have all been converted into Hoot type definitions.
The appropriate Hoot library classes implement the Smalltalk [standard types](libs.md#library-types-and-classes).

**6. Hoot class declarations are messages, with associated class members.**

Here's a (partial) sample from the **Hoot Geometry** package.

```
Hoot Magnitudes Magnitude import.
"... more imports ..."

Magnitude subclass: Point. "A point on a 2-dimensional plane."
Point class members: "creating instances" [ "..." ]
Point "arithmetic" members: [ "..." ]
```

Note that a class definition can **import** types and classes from other packages.
There are several variations of [class definition patterns](model.md#hoot-class-structure) supported by Hoot.
See the Hoot library classes for examples.

**7. Hoot uses Java-style packages for its name spaces.**

The Hoot compiler assigns each class to a Java package based on its location in the file system (relative to a base folder).
While the C# language does not require files to be located in package folders to indicate their name spaces,
the Hoot compiler organizes the C# code it generates in that way also.


```
Copyright 2010,2019 Nikolas S Boyd.
Permission is granted to copy this work provided this copyright statement is retained in all copies.
```
See https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt for LICENSE details.


[bistro]: https://bitbucket.org/nik_boyd/bistro-smalltalk/ "Bistro"
[smalltalk]: https://en.wikipedia.org/wiki/Smalltalk "Smalltalk"
[images]: https://en.wikipedia.org/wiki/Smalltalk#Image-based_persistence "Image Persistence"
[java]: https://en.wikipedia.org/wiki/Java_%28programming_language%29 "Java"
[csharp]: https://en.wikipedia.org/wiki/C_Sharp_%28programming_language%29 "C#"
[antlr]: https://www.antlr.org/ "ANTLR"
[st]: https://www.stringtemplate.org/ "StringTemplate"
[git]: https://git-scm.com/ "Git"
[github]: https://github.com/ "GitHub"
[nexus]: https://www.sonatype.com/nexus "Sonatype Nexus"
[generics]: https://en.wikipedia.org/wiki/Parametric_polymorphism "Generic Types"
[chunk-files]: https://wiki.squeak.org/squeak/1105 "Chunk File Format"
[open-jdk]: https://en.wikipedia.org/wiki/OpenJDK "OpenJDK"
[net-core]: https://en.wikipedia.org/wiki/.NET_Core ".NET Core"

[hoot-ansi]: ANSI-X3J20-1.9.pdf
[squeak-ansi]: https://wiki.squeak.org/squeak/172

[using-natural-language]: http://www.educery.com/papers/rhetoric/road/
[stable-concept]: http://educery.com/educe/patterns/stable-concept.html
[optional]: notes.md#optional-types "Optional Types"
[generics]: notes.md#generic-types "Generics"
