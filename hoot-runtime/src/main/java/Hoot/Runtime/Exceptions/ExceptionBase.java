package Hoot.Runtime.Exceptions;

/**
 * An abstract base (adapter) exception class.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public abstract class ExceptionBase extends RuntimeException {
    
    public static final String[] RootExceptions = { "Throwable", "Exception", "RuntimeException", };

    protected ExceptionBase(String message) { super(message); }
    public Metaclass $class() { return (Metaclass)Metaclass.$class; }

    public static Metaclass type() { return (Metaclass)Metaclass.$class; }
    public static class Metaclass {
        protected java.lang.Class instanceClass;
        static final ExceptionBase.Metaclass $class = new ExceptionBase.Metaclass();
        public Metaclass() { this(ExceptionBase.Metaclass.class); }
        public Metaclass(java.lang.Class aClass) { instanceClass = ExceptionBase.class; }
        public void initialize() { }

    } // Metaclass

} // ExceptionBase
