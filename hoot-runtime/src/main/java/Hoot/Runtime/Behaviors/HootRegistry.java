package Hoot.Runtime.Behaviors;

import java.util.*;

import Hoot.Runtime.Faces.Valued;
import Hoot.Runtime.Names.TypeName;
import static Hoot.Runtime.Names.Keyword.*;
import static Hoot.Runtime.Faces.Utils.*;
import static Hoot.Runtime.Names.TypeName.*;

/**
 * Root runtime class with registries.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public class HootRegistry {

    protected static final HashMap<Class<?>, Object> ValueRegistry = new HashMap();
    public static void registerValue(Object instance) {
        nullOr(item -> ValueRegistry.put(item.getClass(), item), instance); }

    public static <ResultType> ResultType getValue(Class<?> aClass) {
        return (ResultType)nullOr(c -> ValueRegistry.get(c), aClass); }

    protected static final HashMap<Class<?>, Typified> TypeRegistry = new HashMap();
    public static Typified emptyType() { return Mirror.emptyMirror(); }
    public static Typified getType(Class<?> aClass) { return emptyOr(aClass); }
    public static boolean hasType(Class<?> aClass) { return !getType(aClass).isEmpty(); }
    public static Typified emptyOr(Class<?> aClass) { return (hasNo(aClass)) ? emptyType() : emptyOrType(aClass); }
    protected static Typified emptyOrType(Class<?> aClass) { return TypeRegistry.getOrDefault(aClass, emptyType()); }
    public static Typified registerType(Typified type) { 
        if (hasNo(type)) return emptyType(); // vacuous --nik
        if (hasType(type.primitiveClass())) return getType(type.primitiveClass());
        return TypeRegistry.put(type.primitiveClass(), type); }

    static { registerType(Mirror.forClass(JavaRoot)); }
    public static Class<?> JavaRoot() { return JavaRoot; }
    public static Class<?> RootClass() { return itemOr(JavaRoot, RootType().findClass()); }

    public static TypeName ObjectType() { return TypeName.ObjectType(); }
    public static TypeName RootType() { return TypeName.RootType(); }
    public static TypeName ArrayType() { return TypeName.ArrayType(); }

    public static TypeName BooleanType() { return TypeName.fromCache(Hoot, Behaviors, Boolean); }
    public static TypeName BehaviorType() { return TypeName.fromCache(Hoot, Behaviors, Behavior); }
    public static TypeName ClassType() { return TypeName.fromCache(Hoot, Behaviors, ClassType); }
    public static TypeName MetaclassType() { return TypeName.fromCache(Hoot, Behaviors, MetaclassType); }

    public static TypeName NilType() { return TypeName.fromCache(Hoot, Behaviors, Nil); }
    public static TypeName FalseType() { return TypeName.fromCache(Hoot, Behaviors, False); }
    public static TypeName TrueType() { return TypeName.fromCache(Hoot, Behaviors, True); }
    public static TypeName StringType() { return TypeName.fromCache(Hoot, Collections, String); }
    public static TypeName SymbolType() { return TypeName.fromCache(Hoot, Collections, Symbol); }
    public static TypeName CharacterType() { return TypeName.fromCache(Hoot, Magnitudes, Character); }
    public static TypeName IntegerType() { return TypeName.fromCache(Hoot, Magnitudes, SmallInteger); }
    public static TypeName FixedType() { return TypeName.fromCache(Hoot, Magnitudes, Fixed); }
    public static TypeName FloatType() { return TypeName.fromCache(Hoot, Magnitudes, Float); }

    public static Valued Nil() { return getValue(NilType().findClass()); }
    public static Valued True() { return getValue(TrueType().findClass()); }
    public static Valued False() { return getValue(FalseType().findClass()); }

    public static boolean isRunning() { return false; }
    public static boolean isReady() { return false; }

} // HootRegistry
