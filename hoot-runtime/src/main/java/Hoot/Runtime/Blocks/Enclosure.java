package Hoot.Runtime.Blocks;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;

import Hoot.Runtime.Faces.*;
import Hoot.Runtime.Values.Frame;
import static Hoot.Runtime.Faces.Utils.*;
import Hoot.Runtime.Behaviors.HootRegistry;

/**
 * A block enclosure.
 *
 * <h4>Enclosure Responsibilities:</h4>
 * <ul>
 * <li>knows a block</li>
 * <li>knows a block argument count</li>
 * <li>evaluates a block to produce a result</li>
 * </ul>
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public class Enclosure implements NiladicValuable, MonadicValuable, DyadicValuable {

    protected Enclosure(Function<Frame,?> block) { this.block = block; }
    public static Enclosure withBlock(Consumer<Frame> block) {
        if (block == null) throw new IllegalArgumentException("block missing");
        return withBlock(f -> { block.accept(f); return null; }); }

    public static Enclosure withBlock(Function<Frame,?> block) {
        if (block == null) throw new IllegalArgumentException("block missing");
        return new Enclosure(block); }


    private final Function<Frame,?> block;
    public <ValueType> Function<Frame,ValueType> block() { return (Function<Frame,ValueType>)this.block; }

    private Frame frame = new Frame();
    public Frame frame() { return this.frame; }
    public Enclosure withFrame(Frame frame) { this.frame = frame; return this; }
    @Override public IntegerValue argumentCount() { return IntegerValue.Source.with(frame().countArguments()); }

    /**
     * @return a value resulting from the base block, after the termination block is also evaluated
     * @param terminationBlock always evaluated (finally)
     */
    public Valued ensure(Valuable terminationBlock) {
        Valued result = HootRegistry.Nil();
        try {
            result = value();
        }
        finally {
            terminationBlock.value();
        }
        return result;
    }

    /**
     * @return a value resulting either from the base block, or the termination block if the base fails
     * @param terminationBlock evaluated only if an exception occurs (catch)
     */
    public Valued ifCurtailed(Valuable terminationBlock) {
        Valued result = HootRegistry.Nil();
        try {
            result = value();
        }
        catch (Throwable ex) {
            terminationBlock.value();
        }
        return result;
    }

    public <V> boolean testWithEach(Set<V> values) { return values.stream().anyMatch(each -> evaluateWith(each)); }
    public <V> Set<?>  evaluateWithEach(Set<V> values) { return mapSet(values, each -> evaluateWith(each)); }

    public <V> List<?> evaluateWithEach(V ... values) { return evaluateWithEach(wrap(values)); }
    public <V> List<?> evaluateWithEach(List<V> values) { return map(values, each -> evaluateWith(each)); }
    public <V, R> R evaluateWith(V value) {
        try {
            frame().withAll(value);
            return value();
        }
        finally {
            frame().purge();
        }
    }

    @Override public <R> R value() { return (R)block().apply(frame()); }
    @Override public <V, R> R value(V value) { return valueWith(value); }
    @Override public <A, B, R> R value_value(A a, B b) { return valueWith(a, b); }
    public <ResultType> ResultType valueWith(Object... values) {
        try {
            frame().withAll(values);
            return value();
        }
        finally {
            frame().purge();
        }
    }

} // Enclosure
