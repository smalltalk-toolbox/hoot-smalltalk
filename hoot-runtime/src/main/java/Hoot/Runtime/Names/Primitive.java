package Hoot.Runtime.Names;

import java.util.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import static java.lang.reflect.Modifier.STATIC;
import static org.apache.commons.lang3.StringUtils.*;
import org.apache.commons.lang3.StringUtils;

import Hoot.Runtime.Faces.Valued;
import Hoot.Runtime.Faces.Logging;
import Hoot.Runtime.Behaviors.HootRegistry;
import static Hoot.Runtime.Names.Name.Dot;
import static Hoot.Runtime.Names.Keyword.Arrayed;
import static Hoot.Runtime.Faces.Utils.*;

/**
 * Knows primitive Java types.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public class Primitive implements Logging {
    
    public static boolean isStatic(Field f) { return STATIC == (f.getModifiers() & STATIC); }
    public static boolean isStatic(Method m) { return STATIC == (m.getModifiers() & STATIC); }

    public static int length(byte[] bytes) { return bytes.length; }
    public static <T> int length(T[] elements) { return elements.length; }
    public static String reverse(String value) { return StringUtils.reverse(value); }

    public static List<String> tokenize(String value, String separators) {
        return map(Collections.list(tokens(value, separators)), token -> (String)token); }

    public static StringTokenizer tokens(String value, String separators) {
        return new StringTokenizer(value, separators); }

    public static String systemValue(String valueName) { return System.getProperty(valueName); }
    public static String systemValue(String valueName, String defaultValue) {
        return System.getProperty(valueName, defaultValue); }

    private static final int NegativeUnity = -1;
    public static int negativeUnity() { return NegativeUnity; }

    public static void print(String message) { System.out.print(message); }
    public static void printLine(String message) { System.out.println(message); }
    public static void printError(String message) { System.err.println(message); }
    public static String printLong(long longValue) { return Long.toString(longValue); }

    @SuppressWarnings("UseSpecificCatch")
    public static boolean class_hasMethod(Class aClass, String methodName) {
        try {
            Class[] empty = { };
            return (aClass.getMethod(methodName, empty)) != null;
        }
        catch (Exception e) {
            return false;
        }
    }

    public static Object newInstance(Class<?> aClass) {
        try {
            return aClass.newInstance();
        }
        catch (IllegalAccessException | InstantiationException ex) {
            printError(aClass.getCanonicalName() + " can't create a new instance");
            return HootRegistry.Nil();
        }
    }

    public static double truncate(double value) {
        return (value < 0.0d ? Math.ceil(value) : Math.floor(value)); }

    public static int[] fractionalize(double value, int sig) {
        double limit = Math.pow( 10.0d, sig );

        double n1 = truncate(value);
        double n2 = 1.0d;
        double d1 = 1.0d;
        double d2 = 0.0d;

        double i  = n1;
        double x  = 0.0d;
        double f = value - n1;

        while (f != 0.0d) {
            double dn = 1.0d / f;
            i = truncate(dn);
            f = dn - i;

            x = n2;
            n2 = n1;
            n1 = ( n1 * i ) + x;

            x = d2;
            d2 = d1;
            d1 = ( d1 * i ) + x;
            if (limit < d1) {
                if (n2 == 0.0d) {
                    int[] parts = { (int)n1, (int)d1 };
                    return parts;
                }
                else {
                    int[] parts = { (int)n2, (int)d2 };
                    return parts;
                }
            }
        }

        int[] parts = { (int)n1, (int)d1 };
        return parts;
    }

    public static int invertBits(int bits) { return ~bits; }
    public static int xorBits(int a, int b) { return a ^ b; }

    public static Double elementaryMaxDouble() { return java.lang.Double.MAX_VALUE; }
    public static Double elementaryMinDouble() { return java.lang.Double.MIN_VALUE; }

    public static Float elementaryMaxFloat() { return java.lang.Float.MAX_VALUE; }
    public static Float elementaryMinFloat() { return java.lang.Float.MIN_VALUE; }

    public static Long elementaryMaxLong() { return java.lang.Long.MAX_VALUE; }
    public static Long elementaryMinLong() { return java.lang.Long.MIN_VALUE; }

    public static Integer elementaryMaxInteger() { return java.lang.Integer.MAX_VALUE; }
    public static Integer elementaryMinInteger() { return java.lang.Integer.MIN_VALUE; }

    public static Short elementaryMaxShort() { return java.lang.Short.MAX_VALUE; }
    public static Short elementaryMinShort() { return java.lang.Short.MIN_VALUE; }

    public static Boolean elementaryFalse() { return java.lang.Boolean.FALSE; }
    public static Boolean elementaryTrue() { return java.lang.Boolean.TRUE; }

    public static float getFloat(String value) { return Float.parseFloat(value); }
    public static String getLiteral(String value) {
        if (isEmpty(value)) return Empty;
        if (value.startsWith(Dollar)) return quoteWith(SingleQuote, value.substring(1));
        if (value.startsWith(Pound)) return quoteWith(NativeQuote, value.substring(1));
        if (value.startsWith(SingleQuote)) return quoteNatively(value);
        return value; }

    public static final String SingleQuote = "'";
    public static String quoteLiterally(String value) { return quoteWith(SingleQuote, value); }
    public static String trimQuotes(String quote, String value) { return StringUtils.strip(value, quote); }

    public static final String NativeQuote = "\"";
    public static final String DoubledQuotes = "''";
    public static String quoteNatively(String value) {
        return quoteWith(NativeQuote, trimQuotes(SingleQuote, value).replace(DoubledQuotes, SingleQuote)); }

    public static String quoteWith(String quote, String value) {
        return (isEmpty(value) ? quote + quote : quote + value + quote); }

    public static Valued shallowCopyOf(Valued v) { return v; }
    public static Valued deepCopyOf(Valued v) { return v; }

    protected static final String[] ElementPackages = { "java.", "javax.", };
    protected static final List<String> ElementaryPackages = wrap(ElementPackages);

    public static final String[] SerializedNames = { "Cloneable", "Serializable", "CharSequence", };
    public static final List<String> SerializedTypes = wrap(SerializedNames);

    protected static final Map<String, String> ConversionTypes = new HashMap();
    protected static final Map<String, Class> PrimitiveTypes = new HashMap();
    protected static final Map<String, String> PrimitiveWrappers = new HashMap();
    protected static final Map<String, String> PrimitiveUnwrappers = new HashMap();
    protected static final Map<String, String> ElementaryWrappers = new HashMap();
    protected static final Map<String, String> ElementaryUnwrappers = new HashMap();
    protected static final List<String> Primitives = new ArrayList();
    public    static final Class<?> JavaRoot = java.lang.Object.class;

    static {
        initializePrimitiveTypes();
        initializePrimitiveWrappers();
        initializeObjectWrappers();
    }

    protected static void initializePrimitiveTypes() {
        PrimitiveTypes.put("void", void.class);
        PrimitiveTypes.put("boolean", boolean.class);
        PrimitiveTypes.put("byte", byte.class);
        PrimitiveTypes.put("char", char.class);
        PrimitiveTypes.put("int", int.class);
        PrimitiveTypes.put("short", short.class);
        PrimitiveTypes.put("long", long.class);
        PrimitiveTypes.put("float", float.class);
        PrimitiveTypes.put("double", double.class);
        PrimitiveTypes.put("string", String.class);
        PrimitiveTypes.put("decimal", java.math.BigDecimal.class);
    }

    protected static void initializePrimitiveWrappers() {
        PrimitiveWrappers.put("boolean", "Boolean");
        PrimitiveWrappers.put("byte", "Number");
        PrimitiveWrappers.put("char", "Number");
        PrimitiveWrappers.put("short", "Number");
        PrimitiveWrappers.put("int", "Number");
        PrimitiveWrappers.put("long", "Number");
        PrimitiveWrappers.put("float", "Float");
        PrimitiveWrappers.put("double", "Double");

        PrimitiveUnwrappers.put("boolean", "asPrimitive");
        PrimitiveUnwrappers.put("byte", "primitiveByte");
        PrimitiveUnwrappers.put("char", "primitiveCharacter");
        PrimitiveUnwrappers.put("short", "primitiveShort");
        PrimitiveUnwrappers.put("int", "primitiveInteger");
        PrimitiveUnwrappers.put("long", "primitiveLong");
        PrimitiveUnwrappers.put("float", "asPrimitive");
        PrimitiveUnwrappers.put("double", "asPrimitive");

        Primitives.addAll(PrimitiveWrappers.keySet());
        Primitives.add("void");
    }

    protected static void initializeObjectWrappers() {
        ConversionTypes.put("Int", "int");
        ConversionTypes.put("Byte", "byte");
        ConversionTypes.put("java.lang.Byte", "byte");
        ConversionTypes.put("java.lang.Boolean", "boolean");
        ConversionTypes.put("java.lang.String", "java.lang.String");

        ElementaryWrappers.put("java.math.BigDecimal", "Fixed");
        ElementaryWrappers.put("java.lang.Boolean", "Boolean");
        ElementaryWrappers.put("java.lang.Float", "Float");
        ElementaryWrappers.put("java.lang.Double", "Double");
        ElementaryWrappers.put("java.lang.Character", "Character");
        ElementaryWrappers.put("java.lang.String", "String");
        ElementaryWrappers.put(JavaRoot.getCanonicalName(), "Object");

        ElementaryWrappers.put("Boolean", "Boolean");
        ElementaryWrappers.put("String", "String");
        ElementaryWrappers.put("Float", "Float");
        ElementaryWrappers.put("Double", "Double");
        ElementaryWrappers.put("Object", "Object");

        ElementaryUnwrappers.put("java.math.BigDecimal", "asPrimitive");
        ElementaryUnwrappers.put("java.lang.Boolean", "asPrimitive");
        ElementaryUnwrappers.put("java.lang.String", "asPrimitive");

        // can any others be predetermined?
    }

    public static Class typeNamed(String typeName) {
        return PrimitiveTypes.getOrDefault(typeName.toLowerCase(), null); }

    public static String simplifiedType(String typeName) { return typeName.replace(Arrayed, Empty).toLowerCase(); }
    public static Class getPrimitiveType(String typeName) { return PrimitiveTypes.get(typeName); }
    public static String convertType(String typeName) {
        String result = convertsType(typeName) ? ConversionTypes.get(typeName) : typeName;
        return result; }

    public static boolean convertsType(String typeName) {
        return !typeName.isEmpty() && ConversionTypes.containsKey(typeName); }

    public static boolean isPrimitiveType(String typeName) {
        return !typeName.isEmpty() &&
            Primitives.contains(simplifiedType(typeName)) &&
            !ElementaryWrappers.containsKey(typeName); }

    public static boolean isElementaryType(String typeName) {
        return !typeName.isEmpty() && ElementaryWrappers.containsKey(typeName); }

    public static boolean isEraseableType(String typeName) {
        return !typeName.isEmpty() &&
            (PrimitiveWrappers.containsKey(simplifiedType(typeName)) ||
            ElementaryWrappers.containsKey(typeName)); }

    public static boolean fromElementaryPackage(String typeName) {
        return !typeName.isEmpty() && typeName.contains(Dot) ?
            matchAny(ElementaryPackages, prefix -> simplifiedType(typeName).startsWith(prefix)) :
            matchAny(ElementaryPackages, prefix -> prefix.startsWith(simplifiedType(typeName))); }

    public static String[] wrapsFrom(String typeName) {
        if (PrimitiveWrappers.containsKey(typeName)) {
            String[] result = { PrimitiveWrappers.get(typeName), PrimitiveUnwrappers.get(typeName) };
            return result;
        }

        if (ElementaryWrappers.containsKey(typeName)) {
            String[] result = { ElementaryWrappers.get(typeName), ElementaryUnwrappers.get(typeName) };
            return result;
        }

        String[] empty = EmptyArray;
        return empty;
    }


    public static final String Dollar = "$";
    public static final String Pound = "#";
    public static final String Blank = " ";
    public static final String[] EmptyArray = { };
    private static final Primitive Reporter = new Primitive();

} // Primitive
