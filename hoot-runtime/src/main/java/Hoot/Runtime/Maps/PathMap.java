package Hoot.Runtime.Maps;

import java.io.*;
import java.util.*;

import static Hoot.Runtime.Maps.Package.*;
import static Hoot.Runtime.Maps.Library.*;
import static Hoot.Runtime.Names.Operator.Dot;
import Hoot.Runtime.Faces.Logging;

/**
 * Maps the classes located in an element of a class path.
 * Locates all package folders relative to a base path from the class path.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 1999,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public class PathMap implements Logging {

    public PathMap(String folderPath) { basePath = folderPath; }
    protected String basePath; // the absolute base path mapped by this instance

    /**
     * Maps a package folder name to a set of class names.
     */
    protected Map<String, List<String>> map = new HashMap();
    protected Map<String, String> faceMap = new HashMap();

    public void load() { load(Empty); }
    protected void load(String folderName) {
        File directory = new File(basePath, folderName);
        List<String> faceNames = new ArrayList();
        String[] list = directory.list();

        int count = list.length;
        for (int n = 0; n < count; n++) {
            String fileName = list[n];
            File file = new File(directory, fileName);
            if (file.isDirectory()) {
                this.load( // load subdirectory
                    folderName.isEmpty() ? fileName : folderName + Slash + fileName
                );
            }
            else if (fileName.endsWith(ClassFileType)) {
                int length = fileName.length() - ClassFileType.length();
                String shortName = fileName.substring(0, length);
                String packageName = folderName.replace(Slash, Dot);
                Package.named(packageName).loadFace(shortName);
                faceNames.add(shortName);
            }
        }

        if (!faceNames.isEmpty()) {
//            System.out.println();
//            System.out.println("loaded " + faceNames.size() + " from " + directoryName);

            map.put(folderName, faceNames);
            for (String faceName : faceNames) {
                faceMap.put(faceName, folderName);
            }
        }
    }


    /**
     * @return the complete folder located by a relative folderName
     * @param folderName the name of a Java/Hoot package folder
     */
    public File locate(String folderName) {
        File folder = new File(basePath, folderName);
        return (folder.exists() ? folder : null);
    }

    /**
     * @return the names of the classes found in a folder
     * @param folder the name of a Java/Hoot package folder
     */
    public List<String> classesInFolder(String folder) {
        if (map.isEmpty()) load();
        return (map.containsKey(folder) ? map.get(folder) : new ArrayList()); }

    /**
     * @return the names of the classes found in a package
     * @param aPackage a Java/Hoot package
     */
    public List<String> classesInPackage(Package aPackage) {
        return classesInFolder(aPackage.pathname()); }

    public String packageContaining(String faceName) {
        return (!faceMap.containsKey(faceName) ? Empty : faceMap.get(faceName).replace(Slash, Dot)); }

} // PathMap
