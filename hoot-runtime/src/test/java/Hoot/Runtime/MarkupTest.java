package Hoot.Runtime;

import org.junit.*;
import Hoot.Runtime.Emissions.FreeMarkup;
import Hoot.Runtime.Faces.Logging;

/**
 * Confirms proper markup operation.
 *
 * @author nik <nikboyd@sonic.net>
 * @see "Copyright 2010,2019 Nikolas S Boyd."
 * @see "Permission is granted to copy this work provided this copyright statement is retained in all copies."
 * @see <a href="https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt">LICENSE for more details</a>
 */
public class MarkupTest implements Logging {

    @Test
    public void sampleTest() {
        report(FreeMarkup.named("sample.ftl").injectValues().trim());
        report(FreeMarkup.named("sample.ftl").with("sample_text", "a markup example").injectValues().trim());
    }
}
