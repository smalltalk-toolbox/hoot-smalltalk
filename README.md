### Hoot ###

![Hoot Owl][logo]

Hoot is a variation of [Smalltalk][smalltalk] that integrates the best features of Smalltalk
with those of a host language and its VM, such as [Java][java] (and the [JVM][jvm])
or [C#][csharp] (and the [CLR][clr]).
Hoot is an experimental new programming language which builds on experience and understanding gained during
the development of [Bistro][bistro].

Hoot extends some of the ideas pioneered in Bistro:

* the simplicity and expressiveness of Smalltalk,
* support for the standard Smalltalk type protocols,
* support for _optional_ type annotations,
* easy integration with other class libraries (Hoot or otherwise).

Whereas, the Bistro grammar included and supported some Java keywords like `static`, `public`, `private`,
Hoot makes all such code decorations optional annotations, and it uses naming conventions, type inference,
and Lambda functions to support strong dynamic types at runtime.

Hoot does not provide its own virtual machine, like some [Smalltalks][stimps].
Rather, Hoot takes advantage of already existing VMs that have matured over the past few decades.
Also, Hoot does not use image-based persistence for storing its code.
Rather, like many programming languages, it uses simple text files.
This allows developers to use popular tools for text editing and source code version control.

#### Platform Requirements ####

Hoot was originally developed with Java SE 8 [JDK 8][jdk8], partly due to its [Long Term Support][java-lts] LTS
and support for [Lambdas][lambdas].
However, Java SE 11 has since become available, also with LTS and a few nice language enhancements, including
better type inference and support for local [var][inference] declarations.

Hoot has now adopted these capabilities.
These advances in the JDK allow the Hoot compiler to generate simpler Java code.
So, Hoot now requires at least Java SE 11 [JDK][jdk11].
You'll also need [Maven][maven], at least [version 3.5.0][maven-350] is recommended.

#### Building from Sources ####

Clone this repository, and run the following shell command in the base folder:

```
mvn -U -B clean install
```

This will build all the Hoot compiler and runtime components, then generate Java sources from the Hoot sources
for the various Hoot library types and classes, and then run the tests of the samples.
This also resembles how the associated [build pipeline][build] runs in [Docker][maven-docker].

#### Repository Contents ####

This repository contains the Hoot compiler, and its runtime, type and class libraries, and design docs.
Overall, the Hoot compiler performs source-to-source translation (transcoding) from Hoot to a host language.
Then, the Hoot compiler uses a standard host language compiler to translate the intermediate sources
into class files native to the host language runtime.

Most of these discussions reference [Java][java] and how the Hoot compiler translates Hoot into Java.
However, these discussions also generally apply to [C#][csharp] (on the [CLR][clr]), especially as regards
how Hoot maps the Smalltalk [object model][diagram] into a host language and its platform.

The Hoot compiler was built with [ANTLR 4][antlr], and [StringTemplate][st] + [FreeMarker][freemarker].
This project uses [ANTLR][antlr] to generate the Hoot parser from [its grammar][grammar].
The Hoot compiler accepts directions with a command line interface CLI, that it uses to invoke the parser.
The parser recognizes the code in Hoot source files, and builds trees of ST instances using
the [StringTemplate][st] library.
The ST instances then use [code generation templates][code-lib] to output the corresponding host language source code.
The ST code generation is presently wrapped inside FreeMarker templates, which handle the overall class
and type file structures.

The repository is organized into several Maven projects, each of which builds a portion of Hoot overall.

* Hoot Abstractions
* Hoot Runtime Classes
* Hoot Compiler Classes
* Smalltalk Protocol Type Library
* Hoot Class Library
* Hoot Sample Code + Tests

#### Features ####

Many of the following features were originally developed in the context of [Bistro][bistro].
However, Hoot provides several improvements over Bistro.
For example, Hoot has a uniform model for the annotations found in supported host languages
and for the various language-specific code decorations, e.g., `static, public, private`, etc.
Each link below leads to discussions of the specific feature design details.

| Feature     | Description   |
|-------------|---------------|
| [Language Model][model]    | Hoot has a _declarative_ language model. |
| [Name Spaces][spaces]      | Hoot class packages and name spaces are assigned based on folders. |
| [Classes][classes]         | Hoot classes have a hybrid structure based primarily on Smalltalk. |
| [Metaclasses][classes]     | Hoot supports meta-classes like those found in Smalltalk. |
| [Types][types]             | Hoot supports first-class interfaces (as types). |
| [Metatypes][types]         | Hoot types can have associated meta-types. |
| [Access Control][access]   | Hoot code may use access controls: @**Public**, @**Protected**, @**Private**. |
| [Decorations][decor]       | Hoot also supports: @**Abstract**, @**Final**,   @**Static**. |
| [Annotations][notes]       | Hoot translates other annotations into those of its host language. |
| [Optional Types][optional] | Hoot variable and argument type specifications are optional. |
| [Generic Types][generics]  | Hoot supports definition and use of generic types. |
| [Methods][methods]         | Hoot methods resemble those of Smalltalk. |
| [Interoperability][xop]    | Hoot method names become compatible host method names. |
| [Primitives][prims]        | Hoot supports @**Primitive** methods. |
| [Comments][comments]       | Hoot comments are copied into the host language. |
| [Standard Library][lib]    | Hoot includes types that define [ANSI Smalltalk][squeak-ansi] protocols. |
| [Blocks][blocks]           | Hoot blocks are implemented with Java Lambdas. |
| [Exceptions][except]       | Hoot supports both Smalltalk and Java exception handling. |
| [Questions][faq]           | Frequently Asked Questions - FAQ about Hoot |

```
Copyright 2010,2019 Nikolas S Boyd.
Permission is granted to copy this work provided this copyright statement is retained in all copies.
```
See https://gitlab.com/hoot-smalltalk/hoot-smalltalk/tree/master/LICENSE.txt for LICENSE details.


[bistro]: https://bitbucket.org/nik_boyd/bistro-smalltalk/ "Bistro"
[smalltalk]: https://en.wikipedia.org/wiki/Smalltalk "Smalltalk"
[stimps]: https://en.wikipedia.org/wiki/Smalltalk#List_of_implementations "Smalltalk Implementations"

[jdk8]: https://openjdk.java.net/projects/jdk8/
[jdk11]: https://openjdk.java.net/projects/jdk/11/
[java-lts]: https://www.oracle.com/technetwork/java/java-se-support-roadmap.html
[java]: https://en.wikipedia.org/wiki/Java_%28programming_language%29 "Java"
[jvm]: https://en.wikipedia.org/wiki/Java_virtual_machine "Java Virtual Machine"
[lambdas]: https://www.oracle.com/webfolder/technetwork/tutorials/obe/java/Lambda-QuickStart/index.html
[inference]: https://developer.oracle.com/java/jdk-10-local-variable-type-inference

[csharp]: https://en.wikipedia.org/wiki/C_Sharp_%28programming_language%29 "C#"
[clr]: https://en.wikipedia.org/wiki/Common_Language_Runtime "Common Language Runtime"
[st]: https://www.stringtemplate.org/ "StringTemplate"
[antlr]: https://www.antlr.org/ "ANTLR"
[freemarker]: https://freemarker.apache.org/
[maven]: https://maven.apache.org/
[maven-350]: https://maven.apache.org/docs/3.5.0/release-notes.html
[maven-docker]: https://hub.docker.com/_/maven/

[logo]: https://gitlab.com/hoot-smalltalk/hoot-smalltalk/raw/master/hoot-design/hoot-owl.svg "Hoot Owl"

[hoot-ansi]: hoot-design/ANSI-X3J20-1.9.pdf
[squeak-ansi]: http://wiki.squeak.org/squeak/172

[grammar]: hoot-compiler/src/main/antlr4/Hoot/Compiler/Hoot.g4
[code-lib]: hoot-compiler/src/main/resources/CodeTemplates.stg

[build]: .gitlab-ci.yml#L24
[model]: hoot-design/model.md#language-model "Language Model"
[spaces]: hoot-design/libs.md#name-spaces "Name Spaces"
[classes]: hoot-design/libs.md#classes-and-metaclasses "Classes"
[types]: hoot-design/libs.md#types-and-metatypes "Types"
[access]: hoot-design/notes.md#access-controls "Access Controls"
[notes]: hoot-design/notes.md#annotations "Annotations"
[decor]: hoot-design/notes.md#decorations "Decorations"
[optional]: hoot-design/notes.md#optional-types "Optional Types"
[generics]: hoot-design/notes.md#generic-types "Generics"
[methods]: hoot-design/methods.md#methods "Methods"
[comments]: hoot-design/methods.md#comments "Comments"
[xop]: hoot-design/methods.md#interoperability "Interoperability"
[prims]: hoot-design/methods.md#primitive-methods "Primitives"
[lib]: hoot-design/libs.md#library-types-and-classes "Library"
[diagram]: hoot-design/libs.md#type-hierarchy-diagram "Diagram"
[blocks]: hoot-design/blocks.md#blocks "Blocks"
[except]: hoot-design/exceptions.md#exceptions "Exceptions"
[faq]: hoot-design/faq.md#frequently-asked-questions "Questions"
